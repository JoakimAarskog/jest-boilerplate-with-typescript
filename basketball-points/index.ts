const TWO_POINTER_VALUE = 2;
const THREE_POINTER_VALUE = 3;

export function points(twoPointers: number, ThreePointers: number) {
  return twoPointers * TWO_POINTER_VALUE + ThreePointers * THREE_POINTER_VALUE;
}
