import {points} from "."

it("should be 5", () => {
  expect(points(1, 1)).toBe(5);
});

it("should be 29", () => {
  expect(points(7, 5)).toBe(29);
});

it("should be 100", () => {
  expect(points(38, 8)).toBe(100);
});

it("should be 3", () => {
  expect(points(0, 1)).toBe(3);
});

it("should be 0", () => {
  expect(points(0, 0)).toBe(0);
});
