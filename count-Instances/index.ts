export function charCount(character: string, text: string) {
  const tempArray = text.split("");
  let counter = 0;

  for (let i = 0; i < tempArray.length; i++) {
    if (tempArray[i] === character) counter++;
  }

  return counter;
}