import {charCount} from "."

it("should return 1 instance of a", () => {
    expect(charCount("a", "edabit")).toBe(1)
}) 

it("should return 1 instance of c", () => {
    expect(charCount("c", "Chamber of secrets")).toBe(1)
}) 

it("should return 0 instances of B", () => {
    expect(charCount("B", "boxes are fun")).toBe(0)
}) 

it("should return 4 instances of b", () => {
    expect(charCount("b", "big fat bubble")).toBe(4)
}) 

it("should return 0 instances of e", () => {
    expect(charCount("e", "javascript is good")).toBe(0)
}) 

it("should return 2 instances of !", () => {
    expect(charCount("!", "!easy!")).toBe(2)
}) 
